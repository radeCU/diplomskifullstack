export interface Komentar
{
    idKomentara: number,
    idObjave: number,
    idAutora: number,
    korisnickoIme: string,
    sadrzaj: string
}